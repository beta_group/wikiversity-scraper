/**
 * Convert NodeList to array
 * @return {Array} the new array
 */
NodeList.prototype.toArray = function toArray() {
    return Array.prototype.slice.call(this);
};

/**
 * @typedef {Object} task
 * @property {String} name
 * @property {Number|String} score 
 * @property {String} creator
 * @property {String} description
 * @property {String} multi 
 * @property {String} finishDate
 */

const NONE = 0;

const MAIN_PAGE = 1;
const SOLUTION_PAGE = 2;

const CHALLENGE_TABLES = 3;
const CHALLENGE_TABLE = 4;
const CHALLENGE_SOLVED = 5;

/**
 * parse the wikipedia page to beta format if the request was succesful
 * @param {String} data - the html page
 * @returns {Array.<Array.<task>>} list of tables
 */
function parseMain(data) {
    // Convert string to html and get all the tables
    let parser = new DOMParser();
    let doc = parser.parseFromString(data, 'text/html');
    let nodeList = doc.querySelectorAll('table.wikitable');
    let arr = nodeList.toArray();
    arr = arr.filter(x => !x.classList.contains('sortable')).slice(1);

    let tables = [];

    for(let table of arr) {
        let tableBody = table.querySelector('tbody');
        let rows = tableBody.querySelectorAll('tr').toArray().slice(1);
        let output = [];
        for(let row of rows) {
            let cells = row.querySelectorAll('td').toArray();
            let obj = {
                name: cells[0].innerHTML.replace(/<[^>]*>/g, ''),
                score: cells[1].innerHTML,
                creator: cells[2].innerHTML.replace(/<[^>]*>/g, ''),
                description: cells[3].innerHTML,
                multi: cells[4].innerHTML,
                finishDate: cells[5].innerHTML
            };
            output.push(obj);
        }

        tables.push(output);
    }

    return tables;
}

/**
 * parse the wikipedia page to beta format if the request was succesful
 * @param {String} data - the html page
 * @returns {Array}
 */
function parseSolutions(data) {
    // Convert string to html and get all the tables
    let parser = new DOMParser();
    let doc = parser.parseFromString(data, 'text/html');
    let nodeList = doc.querySelectorAll('table.wikitable');
    let arr = nodeList.toArray();

    let table = arr[1];
    let rtnObjs = [];
    let tableBody = table.querySelector('tbody');
    let rows = tableBody.querySelectorAll('tr').toArray().slice(1);
    for(let row of rows) {
        let cells = row.querySelectorAll('td').toArray();
        let obj = {
            name: cells[0].innerHTML,
            users: []
        };

        let pre = cells[1].querySelector('pre');
        let users = pre.innerHTML.split('\n').map(x => x.slice(2).trim().split(' ')[0]);
        users.pop();
        obj.users = users;
        rtnObjs.push(obj);
    }

    return rtnObjs;
}

/**
 * show some stats
 * @param {Array.<Array.<task>>} tables - a list of the tables from the wiki
 */
function stats(tables) {
    let users = {};
    for(let table of tables) {
        for(let row of table) {
            if(!users[row.creator]) {
                users[row.creator] = 1;
            } else {
                users[row.creator] += 1;
            }
        }
    }
    console.log(users);
}

/**
 * this function read a web page and send it to the wiki parser
 * @param {String} url - the url to scrap
 * @param {Number} page - what page to scrap
 * @param {Object} data
 * @returns {void}
 */
function scrap(url, page, data = {type: NONE}) {
    let request = new XMLHttpRequest();
    request.open('GET', url, true);

    request.onload = function() {
        if (request.status >= 200 && request.status < 400) {
            if(page == MAIN_PAGE) {
                let tables = parseMain(request.responseText);
                let data = {
                    type: CHALLENGE_TABLE,
                    table: tables[2]
                };

                scrap('http://192.168.1.11/wikib.php', SOLUTION_PAGE, data);

                // let insert = 'INSERT INTO `challenges` (`category_id`, `name`, `description`, `points`, `author_id`, `repeatable`) VALUES ';
                // for(let i=0; i<table.length; i++) {
                //     let score = Number(table[i].score);
                //     let notes = "";

                //     if(Number.isNaN(score)) {
                //         notes = table[i].score;
                //         score = 0;
                //     }

                //     let repeatable = table[i].multi == 'חד' ? 1 : 0;

                //     insert += `(3, '${table[i].name}', '${table[i].description}', ${score}, ${table[i].creator}, ${repeatable}, ${notes})`;

                //     if(i!==0) {
                //         insert += ", ";
                //     }
                // }

                // stats(tables);
            } else if(page == SOLUTION_PAGE) {
                if(data.type === CHALLENGE_TABLE && data.table) {
                    // Fix score from string to number
                    for(let i=0; i<data.table.length; i++) {
                        let score = Number(data.table[i].score);

                        if(i==1) {
                            score = 1;   
                        }

                        data.table[i].score = score;
                    }

                    let solutions = parseSolutions(request.responseText);
                    let users = [];

                    for(let i=0; i<data.table.length; i++) {
                        let foundTask = false;
                        for(let j=0; j<solutions.length; j++) {
                            if(data.table[i].name == solutions[j].name) {
                                foundTask = true;
                                for(let user of solutions[j].users) {
                                    let found = false;
                                    for(let userObj of users) {
                                        if(user.toLowerCase() == userObj.name.toLowerCase()) {
                                            found = true;
                                            userObj.score += data.table[i].score;
                                        }
                                    }

                                    if(!found) {
                                        users.push({
                                            name: user,
                                            score: data.table[i].score
                                        });
                                    }
                                }
                            }
                        }

                        if(foundTask === false) {
                            console.log(data.table[i].name);
                        }
                    }

                    console.table(users);
                }
            }
        } else {
            console.log(request.status);
        }
    };

    request.onerror = function() {
        console.log('error');
    };

    request.send();
}

scrap('http://192.168.1.11/wiki.php', MAIN_PAGE);
